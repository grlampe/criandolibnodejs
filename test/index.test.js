const pegaArquivo = require('../index');

const arrayResult = [
    {
        FileList: 'https://developer.mozilla.org/pt-BR/docs/Web/API/FileList'
    }
];

describe('pegaArquivo::', () => {

    it('deve ser uma função', () => {
        expect(typeof(pegaArquivo)).toBe('function');
    });

    it('deve retornar um Array com resultados', async () => {
        const restultado = await pegaArquivo('./test/arquivos/texto1.md');

        expect(restultado).toEqual(arrayResult);     
    });

    it('deve retornar uma String dizendo "Não há Links"', async () => {
        const restultado = await pegaArquivo('./test/arquivos/texto1_semlinks.md');

        expect(restultado).toEqual('Não há Links');     
    });

    it('deve lançar um erro na falta de arquivo', async () => {
        await expect(pegaArquivo('./test/arquivos/')).rejects.toThrow("EISDIR Não há arquivo no caminho...")
      })
});