Este Projeto é uma parte dos Estudos da ALURA (Criando uma Biblioteca) em NodeJS.

Esta lib tem como proposito resolver a problematica de validar os links de um arquivo externo em formato markdown.

Dependencias Utilizadas:

 - chalk (para colorir as mensagens de log, erro, etc...)
 - node-fetch (para verificar os status dos links)
 - jest (para criar os testes)